<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    public function login($nrp, $password)
    {
        $user = $this->db->get_where('tb_karyawan', ['nrp' => $nrp])->row_array();
        return $user;
    }

    public function success_login()
    {
        $user = $this->db->get_where('tb_karyawan', ['nrp' => $this->session->userdata('username')])->row_array();
        return $user;
    }
}
