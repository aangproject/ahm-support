<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $_table = "user";
    public $id;


    public function view()
    {
        $user = $this->db->get('user')->result_array();
        return $user;
    }

    public function getById($id)
    {
        $query = $this->db->escape($this->db->get_where('user', array('id' => $id)));
        return $query->row_array();
    }

    public function tambah()
    {
        $this->id = "";
        $data = [
            'nama' => htmlspecialchars($this->input->post('nama', true)),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'status' => htmlspecialchars($this->input->post('status', true)),
            'level' => htmlspecialchars($this->input->post('level', true)),

         ];

        return $this->db->insert('user', $data);

    }

    public function ubah()
    {
          $post = $this->input->post();
          $this->id = $post["id"];
          $this->nama = $post["nama"];
          $this->username = $post["username"];
          $this->email = $post["email"];
          $this->status = $post["status"];
          $this->level = $post["level"];
          $this->db->update($this->_table, $this, array('id' => $post["id"]));
    }

  public function hapus($id)
  {
      return $this->db->delete($this->_table, array("id" => $id));
  }
}
