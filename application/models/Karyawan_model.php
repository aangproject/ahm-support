<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
  public function login($nrp, $password)
  {
      $user = $this->db->get_where('tb_karyawan', ['nrp' => $nrp])->row_array();
      return $user;
  }

  public function success_login()
  {
      $user = $this->db->get_where('tb_karyawan', ['nrp' => $this->session->userdata('username')])->row_array();
      return $user;
  }
  public function view()
  {
    $this->db->select('*, tb_jabatan.id as idjabatan, tb_karyawan.id as idkar,tb_seksi.id as idseksi');
    $this->db->from('tb_karyawan');
    $this->db->join('tb_jabatan', 'tb_jabatan.id = tb_karyawan.id_jabatan');
    $this->db->join('tb_seksi', 'tb_seksi.id = tb_karyawan.id_seksi');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_karyawan');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_karyawan',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_karyawan', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_karyawan', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_karyawan', $data, array('id' => $id));
  }

}
