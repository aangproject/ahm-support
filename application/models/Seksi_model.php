<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Seksi_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*, tb_gedung.id as idgedung, tb_plant.id as idplant, tb_seksi.id as idseksi');
    $this->db->from('tb_seksi');
    $this->db->join('tb_gedung', 'tb_gedung.id = tb_seksi.id_gedung');
    $this->db->join('tb_plant', 'tb_plant.id = tb_seksi.id_plant');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_seksi');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_seksi',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_seksi', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_seksi', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_seksi', $data, array('id' => $id));
  }

}
