<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reject extends CI_Controller {
  function __construct()
  {
      parent::__construct();
      //load model
      $this->load->model('Part_model');
      $this->load->model('Reject_model');
        $this->load->model('Auth_model');
      if (!$this->session->userdata('username')) {
              $this->session->set_flashdata('error', 'Anda belum melakukan login!');
              redirect('auth');
          }

      if ($this->session->userdata('level') == 2) {
          redirect('admin/blank');
      }

  }
	public function index()
	{
    $data['title'] = "Reject";
      $data['user'] = $this->Auth_model->success_login();
    $data['view'] = $this->Reject_model->viewall();
		$this->load->view('templates/header',$data);
    $this->load->view('reject/index');
    $this->load->view('templates/footer');
	}
  public function tambah()
  {
    $data['title'] = 'Admin | Tambah Reject';
  $data['user'] = $this->Auth_model->success_login();
    $data['part'] = $this->Part_model->view();

    $this->form_validation->set_rules('nama_reject', 'Nama Reject','required|trim',  [
        'required' => 'Nama Reject tidak boleh kosong!'
    ]);
    $this->form_validation->set_rules('js_part', 'Jenis Part','required|trim',  [
        'required' => 'Jenis Part tidak boleh kosong!'
    ]);
    if  ($this->form_validation->run() == false) {
        $this->load->view('templates/header', $data);
        $this->load->view('reject/tambah', $data);
        $this->load->view('templates/footer');
        // $error = array('error' => $this->upload->display_error());
    } else {
        $this->Reject_model->tambah();
        $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
        redirect('reject');
    }
  }
  public function ubah($id = null)
  {
      $data['view'] = $this->Reject_model->getById($id);
      $data['part'] = $this->Part_model->view();
        $data['user'] = $this->Auth_model->success_login();
      $data['title'] = 'Admin | Ubah Reject';
      $this->form_validation->set_rules('nama_reject', 'Nama Reject','required|trim',  [
          'required' => 'Nama Reject tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('js_part', 'Jenis Part','required|trim',  [
          'required' => 'Jenis Part tidak boleh kosong!'
      ]);

      if ($this->form_validation->run() == false) {
        $this->load->view('templates/header', $data);
        $this->load->view('reject/ubah', $data);
        $this->load->view('templates/footer');
      } else {
          $this->Reject_model->ubah($id);
          $this->session->set_flashdata('success', 'Data berhasil diubah!');
          redirect('reject');
      }
  }
  public function hapus($id)
  {
      $this->Reject_model->hapus($id);
      $this->session->set_flashdata('success', 'Data berhasil dihapus');
      redirect('reject');
  }

}
