<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Part_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('tb_part');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah()
  {
    $data =[
      'nama_part' => $this->input->post('nama_part'),
      'tipe_part' => $this->input->post('tipe_part'),
    ];
    return $this->db->insert('tb_part',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_part', array("id_part" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_part', array('id_part' => $id)));
    return $query->row_array();
  }
  public function ubah($id)
  {
    $data =[
      'nama_part' => $this->input->post('nama_part'),
      'tipe_part' => $this->input->post('tipe_part'),
    ];
    $id_part = $this->input->post('id_part');
    return $this->db->update('tb_part', $data, array('id_part' => $id_part));
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_part');
    $this->db->join('tb_tipe', 'tb_tipe.id_tipe = tb_part.tipe_part');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }

}
