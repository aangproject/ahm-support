<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Line_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*, tb_line.id as idline');
    $this->db->from('tb_line');
    $this->db->join('tb_station', 'tb_station.id = tb_line.id_station');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_line');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_line',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_line', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_line', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_line', $data, array('id' => $id));
  }

}
