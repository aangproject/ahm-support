<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Beranda_model extends CI_Model
{
  public function sum_reject()
  {
    $this->db->select_sum('reject');
    $query = $this->db->get('oee');
    if($query->num_rows()>0)
    {
      return $query->row()->reject;
    }
    else
    {
      return 0;
    }
  }
  public function sum_actual()
  {
    $this->db->select_sum('actual');
    $query = $this->db->get('oee');
    if($query->num_rows()>0)
    {
      return $query->row()->actual;
    }
    else
    {
      return 0;
    }
  }
  public function sum_ok()
  {
    $actual = $this->db->select_sum('actual');
    $reject = $this->db->select_sum('reject');
    $query = $this->db->get('oee');
    if($query->num_rows()>0)
    {
      $total = ($query->row()->actual) - ($query->row()->reject);
      return $total;
    }
    else
    {
      return 0;
    }
  }

}
