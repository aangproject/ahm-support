<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Job_harian_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*,tb_job_harian.id as id_harian');
    $this->db->from('tb_job_harian');
    $this->db->join('tb_job', 'tb_job.id = tb_job_harian.id_job');
    $this->db->join('tb_shift', 'tb_shift.id = tb_job.id_shift');
    $this->db->join('tb_karyawan', 'tb_karyawan.id = tb_job.id_karyawan');
    $this->db->join('tb_mesin', 'tb_mesin.id = tb_job_harian.id_mesin');
    $this->db->join('tb_type', 'tb_type.id = tb_mesin.id_type');
    $this->db->join('tb_line', 'tb_line.id = tb_mesin.id_line');
    $this->db->join('tb_station', 'tb_station.id = tb_line.id_station');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_job_harian');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_job_harian',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_job_harian', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_job_harian', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_job_harian', $data, array('id' => $id));
  }

}
