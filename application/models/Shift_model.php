<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Shift_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('tb_shift');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_shift');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_shift',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_shift', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_shift', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_shift', $data, array('id' => $id));
  }

}
