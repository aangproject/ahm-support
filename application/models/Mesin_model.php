<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mesin_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*, tb_mesin.id as idmesin');
    $this->db->from('tb_mesin');
    $this->db->join('tb_type', 'tb_type.id = tb_mesin.id_type');
    $this->db->join('tb_line', 'tb_line.id = tb_mesin.id_line');
    $this->db->join('tb_station', 'tb_station.id = tb_line.id_station');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_mesin');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_mesin',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_mesin', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_mesin', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_mesin', $data, array('id' => $id));
  }
  public function checkIfExist($id_line)
  {
    $this->db->select('id_line');
    $this->db->from('tb_mesin');
    $this->db->where('id_line',$id_line);
    $query = $this->db->escape($this->db->get());
    return $query->row_array();

  }

}
