<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Trouble_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*, tb_downtime.id as iddowntime,  tb_trouble.id as idtr');
    $this->db->from('tb_trouble');
    $this->db->join('tb_downtime', 'tb_downtime.id = tb_trouble.id_downtime');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_trouble');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_trouble',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_trouble', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_trouble', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_trouble', $data, array('id' => $id));
  }

}
