<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Plant_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('tb_plant');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_plant');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_plant',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_plant', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_plant', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_plant', $data, array('id' => $id));
  }

}
