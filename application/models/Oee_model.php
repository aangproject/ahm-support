<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Oee_model extends CI_Model
{

  public function tambah($data)
  {
    return $this->db->insert('tb_oee_gdc  ',$data);
  }
  public function view($nrp)
  {
    $this->db->select('*');
    $this->db->from('tb_oee_gdc');
    $this->db->where('nrp',$nrp);
    $this->db->order_by('id','desc');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function job($getId)
  {
    $this->db->select('*,tb_job_harian.id as id_harian');
    $this->db->from('tb_job_harian');
    $this->db->join('tb_job', 'tb_job.id = tb_job_harian.id_job');
    $this->db->join('tb_shift', 'tb_shift.id = tb_job.id_shift');
    $this->db->join('tb_karyawan', 'tb_karyawan.id = tb_job.id_karyawan');
    $this->db->join('tb_mesin', 'tb_mesin.id = tb_job_harian.id_mesin');
    $this->db->join('tb_type', 'tb_type.id = tb_mesin.id_type');
    $this->db->join('tb_line', 'tb_line.id = tb_mesin.id_line');
    $this->db->join('tb_station', 'tb_station.id = tb_line.id_station');
    $this->db->where('id_job',$getId);
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function getIdJob($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_job', array('id_karyawan' => $id)));
    return $query->row_array();
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_job_harian', array('id' => $id)));
    return $query->row_array();
  }
  public function getLastId()
  {
    $this->db->select('*');
    $this->db->from('tb_oee_gdc');
    $this->db->order_by('id','desc');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }

}
