<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Type_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('tb_type');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_type');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_type',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_type', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_type', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_type', $data, array('id' => $id));
  }

}
