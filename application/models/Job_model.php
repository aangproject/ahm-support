<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Job_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*,tb_job.id as idjob ');
    $this->db->from('tb_job');
    $this->db->join('tb_karyawan', 'tb_karyawan.id = tb_job.id_karyawan');
    $this->db->join('tb_shift', 'tb_shift.id = tb_job.id_shift');
    $this->db->join('tb_station', 'tb_station.id = tb_job.id_station');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('tb_job');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('tb_job',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('tb_job', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('tb_job', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('tb_job', $data, array('id' => $id));
  }

}
