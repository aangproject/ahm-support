<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Data OEE</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <?php foreach ($job as $row) : ?>
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6 border">
                <table class="table no-border">
                  <tr>
                    <td>
                      <h5>NRP</h5>
                    </td>
                    <td>:</td>
                    <td><?= $user['nrp']; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Nama</h5>
                    </td>
                    <td>:</td>
                    <td><?= $user['nama']; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Shift</h5>
                    </td>
                    <td>:</td>
                    <td><?= $row['nama_shift']; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Station</h5>
                    </td>
                    <td>:</td>
                    <td><?= $row['nama_station']; ?></td>
                  </tr>
                </table>
              </div>
              <div class="col-sm-6 border border-left-dark">
                <table class="table no-border">
                  <tr>
                    <td>
                      <h5>No Mesin</h5>
                    </td>
                    <td>:</td>
                    <td><?= $row['no_mesin']; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Type</h5>
                    </td>
                    <td>:</td>
                    <td><?= $row['nama_type']; ?></td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Dise</h5>
                    </td>
                    <td>:</td>
                    <td><?= $row['dies']; ?></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <button data-toggle="modal" data-target="#modalTambah" class="btn btn-primary btn-sm float-right mb-2"><i class="fa fa-plus"></i>&nbsp; Tambah </button>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Actual</th>
                    <th>Sozai</th>
                    <th>NG</th>
                    <th>Downtime</th>
                    <th>OK</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody id="tampildata">

                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<form id="tambah" method="post">
  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Input OEE</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <center>
            <font color="red">
              <p id="pesan"></p>
            </font>
          </center>
          <div class="form-group">
            <label for="exampleInputEmail1">Actual</label>
            <input type="number" name="actual" class="form-control" id="actual">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Sozai</label>
            <input type="number" name="sozai" class="form-control" id="sozai">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">NG</label>
            <input type="number" name="ng" class="form-control" id="ng">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Downtime</label>
            <input type="number" name="downtime" class="form-control" id="downtime">
          </div>
          <input type="text" name="nama" hidden value="<?= $user['nama']; ?>">
          <input type="text" name="nrp" hidden value="<?= $user['nrp']; ?>">
          <input type="text" name="shift" hidden value="<?= $row['nama_shift']; ?>">
          <input type="text" name="station" hidden value="<?= $row['nama_station']; ?>">
          <input type="text" name="no_mesin" hidden value="<?= $row['no_mesin']; ?>">
          <input type="text" name="type" hidden value="<?= $row['nama_type']; ?>">
          <input type="text" name="dies" hidden value="<?= $row['dies']; ?>">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Ubah-->
<form id="ubah" method="post">
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Ubah OEE</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <center>
            <font color="red">
              <p id="e_pesan"></p>
            </font>
          </center>
          <div class="form-group" hidden>
            <label class="col-form-label">Id :</label>
            <input type="text" class="form-control" id="id_mesin" name="id_mesin">
          </div>
          <div class="form-group">
            <label>Line</label>
            <select class="form-control" name="e_id_line" id="e_id_line">
            </select>
          </div>
          <div class="form-group">
            <label>Type</label>
            <select class="form-control" name="e_id_type" id="e_id_type">
            </select>
          </div>
          <div class="form-group">
            <label>Dies</label>
            <input type="number" class="form-control" name="e_dies" id="e_dies" value="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Ubah</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!-- Large modal -->
<form class="" method="post" id="detail">
  <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">KUESIONER</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <?php
            $result = array();
            foreach ($jawaban as $element) {
              $result[$element['pertanyaan']][] = $element;
            }
            ?>
            <div class="col-md-12">
              <?php foreach ($result as $key => $value) : ?>
                <table border="0">
                  <tr>
                    <td>
                      <strong>
                        <p><?= $key; ?></p>
                      </strong>
                    </td>
                  </tr>
                  <?php foreach ($value as $k => $v) : ?>
                    <tr>
                      <td>
                        <input type="radio" name="<?= $v['kuesioner']; ?>" value="<?= $v['id_jawaban']; ?>" required>&nbsp;<?= $v['jawaban']; ?>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                <?php endforeach; ?>
                </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function() {
    // Summernote
    $('.textarea').summernote();
    view();
    getJob();
  });
  $(function() {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

  function getJob() {
    $.ajax({
      type: 'post',
      dataType: 'json',
      url: '<?= base_url() . 'oee/result' ?>',
      success: function(data) {

      }
    });
  }

  function erase() {
    document.getElementById("actual").value = "";
    document.getElementById("ng").value = "";
    document.getElementById("sozai").value = "";
  }
  $('#tambah').on('submit', function(event) {
    event.preventDefault();
    $.ajax({
      type: 'POST',
      url: "<?= base_url() . 'oee/tambah' ?>",
      data: $(this).serialize(),
      dataType: 'json',
      success: function(data) {
        $('#pesan').html(data.pesan);
        if (data.pesan == '') {

          Swal.fire({
            title: 'Berhasil ',
            text: 'Data berhasil disimpan!',
            type: 'success'
          });
          setTimeout(function() {
            $("[data-dismiss=modal]").trigger({
              type: "click"
            });
          }, 100)
          view();
          erase();
        }
      }
    })
  });

  function edit(id) {
    $.ajax({
      type: "POST",
      data: 'id=' + id,
      url: '<?= base_url() . 'oee/getById' ?>',
      dataType: 'json',
      success: function(data) {
        $('[name="id_mesin"]').val(data.id);
        $('[name="e_id_line"]').val(data.id_line);
        $('[name="e_id_type"]').val(data.id_type);
        $('[name="e_dies"]').val(data.dies);

      }
    });
  }
  $('#ubah').on('submit', function(event) {
    event.preventDefault();
    $.ajax({
      url: "<?= base_url() . 'oee/ubah' ?>",
      type: "POST",
      data: $(this).serialize(),
      dataType: 'json',
      success: function(data) {
        $('#e_pesan').html(data.pesan);
        if (data.pesan == '') {
          Swal.fire({
            title: 'Berhasil ',
            text: 'Data berhasil diubah!',
            type: 'success'
          });
          setTimeout(function() {
            $("[data-dismiss=modal]").trigger({
              type: "click"
            });
          }, 100)
          view();
        }
      }
    })
  });

  function view() {
    $.ajax({
      type: 'POST',
      url: '<?= base_url() . 'oee/view' ?>',
      dataType: 'json',
      async: false,
      success: function(data) {
        var baris = '';
        var n = '';
        for (var i = 0; i < data.length; i++) {
          n = i + 1;
          baris += '<tr>' +
            '<td>' + n + '</td>' +
            '<td>' + data[i].actual + '</td>' +
            '<td>' + data[i].sozai + '</td>' +
            '<td>' + data[i].ng + '<a onclick="edit(' + data[i].id + ')" data-toggle="modal" data-target="#detail" class="float-right btn btn-default btn-outline-success btn-sm"><i class="fa fa-edit"></i> Detail</a></td>' +
            '<td>' + data[i].downtime + '<a onclick="edit(' + data[i].id + ')" data-toggle="modal" data-target="#detail" class="float-right btn btn-default btn-outline-success btn-sm"><i class="fa fa-edit"></i> Detail</a></td>' +
            '<td>' + data[i].ok + '</td>' +

            '<td><a onclick="edit(' + data[i].id + ')" data-toggle="modal" data-target="#modalEdit" class="btn btn-default btn-outline-success btn-sm"><i class="fa fa-edit"></i></a><a onclick="hapus(' + data[i].id + ')" class="btn btn-default btn-outline-danger btn-sm"><i class="fa fa-trash"></i></a></td>' +
            '</tr>';
        }
        $('#tampildata').html(baris);

      }
    });
  }

  function hapus(id) {
    Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          type: 'POST',
          data: 'id=' + id,
          url: '<?= base_url() . 'oee/hapus' ?>',
          success: function() {
            Swal.fire({
              title: 'Berhasil ',
              text: 'Data berhasil dihapus!',
              type: 'success'
            });
            view();
          }
        });
      }
    });
  }
</script>