<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Kelola Seksi</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Seksi  &nbsp; <i class=""></i></h3>
              <button data-toggle="modal" data-target="#modalTambah"  class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i>&nbsp; Tambah </button>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Plant</th>
                  <th>Gedung</th>
                  <th>Seksi</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody id="tampildata">

                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<form id="tambah" method="post">
  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Tambah Seksi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <center><font color="red"><p id="pesan"></p></font></center>
          <div class="form-group">
            <label>Plant</label>
            <select class="form-control" name="id_plant" id="id_plant">
            </select>
          </div>
          <div class="form-group">
            <label>Gedung</label>
            <select class="form-control" name="id_gedung" id="id_gedung">
            </select>
          </div>
          <div class="form-group">
            <label>Nama Seksi</label>
            <input type="text" class="form-control" name="nama_seksi" id="nama_seksi" value="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Ubah-->
<form id="ubah" method="post">
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Ubah Shift</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="e_pesan"></p></font></center>
            <div class="form-group" hidden>
              <label class="col-form-label">Id :</label>
              <input type="text" class="form-control" id="id_seksi" name="id_seksi">
            </div>
            <div class="form-group">
              <label>Plant</label>
              <select class="form-control" name="e_id_plant" id="e_id_plant">
              </select>
            </div>
            <div class="form-group">
              <label>Gedung</label>
              <select class="form-control" name="e_id_gedung" id="e_id_gedung">
              </select>
            </div>
            <div class="form-group">
              <label>Nama Seksi</label>
              <input type="text" class="form-control" name="e_nama_seksi" id="e_nama_seksi" value="">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Ubah</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  // Summernote
  $('.textarea').summernote();
   view();
   selectPlant();
   selectGedung();
});
$(function () {
  $("#example1").DataTable();
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
  });
});
function selectPlant() {
  $.ajax({
    type:'post',
    dataType:'json',
    url:'<?= base_url().'seksi/getPlant'?>',
    success:function (data) {
        var html ='';
        var i;
        for (var i = 0; i < data.length; i++) {
          html += '<option value="'+data[i].id+'">'+data[i].nama_plant+'</option>';
        }
        $('#id_plant').html(html);
        $('#e_id_plant').html(html);

    }
  });
}
function selectGedung() {
  $.ajax({
    type:'post',
    dataType:'json',
    url:'<?= base_url().'seksi/getGedung'?>',
    success:function (data) {
        var html ='';
        var i;
        for (var i = 0; i < data.length; i++) {
          html += '<option value="'+data[i].id+'">'+data[i].nama_gedung+'</option>';
        }
        $('#id_gedung').html(html);
        $('#e_id_gedung').html(html);
    }
  });
}
function erase() {
  document.getElementById("id_plant").value = "";
  document.getElementById("id_gedung").value = "";
  document.getElementById("nama_seksi").value = "";
}
$('#tambah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          type:'POST',
          url:"<?= base_url().'seksi/tambah'?>",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#pesan').html(data.pesan);
            if (data.pesan=='') {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil disimpan!',
                  type: 'success'
              });
              setTimeout(function() {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
              view();
              erase();
            }

          }
      })
});
function edit(id) {
  $.ajax({
    type:"POST",
    data:'id='+id,
    url:'<?= base_url().'seksi/getById'?>',
    dataType:'json',
    success:function(data) {
      $('[name="id_seksi"]').val(data.id);
        $('[name="e_id_plant"]').val(data.id_plant);
        $('[name="e_id_gedung"]').val(data.id_gedung);
        $('[name="e_nama_seksi"]').val(data.nama_seksi);

    }
  });
}
$('#ubah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          url:"<?= base_url().'seksi/ubah'?>",
          type:"POST",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#e_pesan').html(data.pesan);
            if (data.pesan=='') {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil diubah!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
                view();
          }
        }
      })
});
function view() {
  $.ajax({
    type:'POST',
    url:'<?= base_url().'seksi/view'?>',
    dataType:'json',
    async:false,
    success:function(data){
      var baris ='';
      var n='';
      for(var i=0;i<data.length;i++){
        n=i+1;
        baris += '<tr>'+
                      '<td>'+  n +'</td>'+
                      '<td>'+ data[i].nama_plant+'</td>'+
                      '<td>'+ data[i].nama_gedung+'</td>'+
                      '<td>'+ data[i].nama_seksi+'</td>'+
                      '<td><a onclick="edit('+ data[i].idseksi+')" data-toggle="modal" data-target="#modalEdit" class="btn btn-default btn-outline-success btn-sm"><i class="fa fa-edit"></i></a><a onclick="hapus('+ data[i].idseksi+')" class="btn btn-default btn-outline-danger btn-sm"><i class="fa fa-trash"></i></a></td>'+
                 '</tr>';
      }
      $('#tampildata').html(baris);
    }
  });
}
function hapus(id) {
  Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'seksi/hapus'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil dihapus!',
                type: 'success'
            });
              view();
          }
        });
      }
  });
}
</script>
