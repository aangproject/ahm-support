<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Kelola User</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data User  &nbsp; <i class=""></i></h3>
              <a href="<?= base_url('user/tambah'); ?>"  class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i>&nbsp; Tambah</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php $no = 0; foreach($view as $row) : $no++ ?>
                    <tr>
                      <td><?= $no; ?></td>
                      <td><?= $row['nama']; ?></td>
                      <td><?= $row['username']; ?></td>
                      <td><?= $row['email']; ?></td>
                      <?php if ($row['status'] == 1): ?>
                        <td>Aktif</td>
                      <?php else : ?>
                        <td>Non-Aktif</td>
                      <?php endif; ?>
                      <td>
                        <a href="<?= base_url('user/ubah/'); ?><?= $row['id']; ?>" class="btn-circle btn-success btn-xs"><i class="fas fa-edit"></i></a>
                        <a href="<?= base_url('user/hapus/'); ?><?= $row['id']; ?>" class="btn-circle btn-danger btn-xs tombol-hapus"><i class="fas fa-trash"></i></a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
