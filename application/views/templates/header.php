<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">

  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- jQuery -->
  <script src="<?= base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <script src="<?= base_url(); ?>assets/plugins/jquery/jquery.js"></script>
  <!-- SweetAlert2 -->
  <script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert2.all.min.js"></script>

  <!-- DataTables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

</head>

<?php
$hal= $this->uri->segment(1);
$hal2= $this->uri->segment(2);

?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href=""><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="">
          <span>Hello ! <?= $user['nama'];?></span>&nbsp;
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <center>  <span class="brand-text font-weight-light">E-REPORT</span></center>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?= base_url('beranda'); ?>" class="nav-link ">
              <p>
                Dashboard 
              </p>
            </a>
          </li>
          
          <?php if(($user['id_jabatan']==1) || ($user['id_jabatan']==2) || ($user['id_jabatan']==3) || ($user['id_jabatan']==4)):?>
          <li class="nav-item <?= ($hal=='karyawan') || ($hal=='plant') || ($hal=='gedung') || ($hal=='seksi') || ($hal=='jabatan') || ($hal=='job') ? 'menu-open' : ''; ?>">
            <a href="#" class="nav-link <?= ($hal=='karyawan') || ($hal=='plant') || ($hal=='gedung') || ($hal=='seksi') || ($hal=='jabatan') || ($hal=='job') ? 'active' : ''; ?>">
              <p>Data Karyawan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('karyawan'); ?>" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                  <p>Karyawan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('plant'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Plant</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('gedung'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Gedung</p>
                </a>
              </li><li class="nav-item">
                <a href="<?= base_url('seksi'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Section</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('jabatan'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jabatan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('job'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Job</p>
                </a>
              </li>
            </ul>
          </li>
          <?php endif;?>
          <li class="nav-item <?= ($hal=='job_harian') || ($hal=='station') || ($hal=='line') || ($hal=='mesin') || ($hal=='type') || ($hal=='oee') || ($hal=='trouble') || ($hal=='downtime') ? 'menu-open' : ''; ?>">
            <a href="#" class="nav-link <?= ($hal=='job_harian') || ($hal=='station') || ($hal=='line') || ($hal=='mesin') || ($hal=='type') || ($hal=='oee') || ($hal=='trouble') || ($hal=='downtime') ? 'active' : ''; ?>">
              <p>Data Produksi
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('job_harian'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Job Harian</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('station'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Station</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('line'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Line</p>
                </a>
              </li><li class="nav-item">
                <a href="<?= base_url('mesin'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Mesin</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('type'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Type</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('oee'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OEE</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('downtime'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Downtime</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('trouble'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Trouble</p>
                </a>
              </li>
            </ul>
          </li>

          
          
        
          <li class="nav-item">
            <a href="<?php echo base_url('auth/logout'); ?>" class="nav-link tombol-logout">
              <p>
                Log Out
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
