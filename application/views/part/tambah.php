<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Kelola Part  &nbsp; <i class=""></i></h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tambah Part  &nbsp; <i class=""></i></h3>
              </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php if (validation_errors()) : ?>
                  <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                      <?= validation_errors(); ?>
                  </div>
              <?php endif; ?>
              <?php echo form_open_multipart('part/tambah'); ?>
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Part</label>
                      <input type="text" name="nama_part" class="form-control" id="exampleInputEmail1">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tipe</label>
                    <select class="form-control" name="tipe_part">
                      <option value="">-- Pilih --</option>
                      <?php foreach ($tipe as $row ) : ?>
                        <option value="<?= $row['id_tipe']?>"><?= $row['nama_tipe'];?></option>
                      <?php  endforeach; ?>
                    </select>
                  </div>
                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
