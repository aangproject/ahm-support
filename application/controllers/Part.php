<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Part extends CI_Controller {
  function __construct()
  {
      parent::__construct();
      //load model
      $this->load->model('Part_model');
      $this->load->model('Tipe_model');
        $this->load->model('Auth_model');
      if (!$this->session->userdata('username')) {
              $this->session->set_flashdata('error', 'Anda belum melakukan login!');
              redirect('auth');
          }

      if ($this->session->userdata('level') == 2) {
          redirect('admin/blank');
      }

  }
	public function index()
	{
    $data['user'] = $this->Auth_model->success_login();
    $data['title'] = "Part";
    $data['view'] = $this->Part_model->viewall();
		$this->load->view('templates/header',$data);
    $this->load->view('part/index');
    $this->load->view('templates/footer');
	}
  public function tambah()
  {
    $data['title'] = 'Admin | Tambah Part';
  $data['user'] = $this->Auth_model->success_login();
    $data['tipe'] = $this->Tipe_model->view();

    $this->form_validation->set_rules('nama_part', 'Nama Part','required|trim',  [
        'required' => 'Nama Part tidak boleh kosong!'
    ]);
    $this->form_validation->set_rules('tipe_part', 'Tipe Part','required|trim',  [
        'required' => 'Tipe Part tidak boleh kosong!'
    ]);
    if  ($this->form_validation->run() == false) {
        $this->load->view('templates/header', $data);
        $this->load->view('part/tambah', $data);
        $this->load->view('templates/footer');
        // $error = array('error' => $this->upload->display_error());
    } else {
        $this->Part_model->tambah();
        $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
        redirect('part');
    }
  }
  public function ubah($id = null)
  {
      $data['view'] = $this->Part_model->getById($id);
        $data['user'] = $this->Auth_model->success_login();
      $data['tipe'] = $this->Tipe_model->view();
      $data['title'] = 'Admin | Ubah Part';
      $this->form_validation->set_rules('nama_part', 'Nama Part','required|trim',  [
          'required' => 'Nama Part tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('tipe_part', 'Tipe Part','required|trim',  [
          'required' => 'Tipe Part tidak boleh kosong!'
      ]);

      if ($this->form_validation->run() == false) {
        $this->load->view('templates/header', $data);
        $this->load->view('part/ubah', $data);
        $this->load->view('templates/footer');
      } else {
          $this->Part_model->ubah($id);
          $this->session->set_flashdata('success', 'Data berhasil diubah!');
          redirect('part');
      }
  }
  public function hapus($id)
  {
      $this->Part_model->hapus($id);
      $this->session->set_flashdata('success', 'Data berhasil dihapus');
      redirect('part');
  }

}
