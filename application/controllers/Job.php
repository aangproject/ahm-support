<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Job extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Job_model');
		$this->load->model('Station_model');
		$this->load->model('Shift_model');
    $this->load->model('Karyawan_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "JOB";
    $this->load->view('templates/header',$data);
    $this->load->view('job/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Job_model->view();
		echo json_encode($data);
	}
	public function getStation()
	{
		$data = $this->Station_model->view();
		echo json_encode($data);
	}
	public function getShift()
	{
		$data = $this->Shift_model->view();
		echo json_encode($data);
	}
  public function getKaryawan()
	{
		$data = $this->Karyawan_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $id_karyawan	 = $this->input->post('id_karyawan');
    $id_station	 = $this->input->post('id_station');
    $id_shift	 = $this->input->post('id_shift');
    if ($id_karyawan == '') {
      $result['pesan'] ="Karyawan Harus Diisi";
    }elseif ($id_station == '') {
      $result['pesan'] ="Station Harus Diisi";
    }elseif ($id_shift == '') {
      $result['pesan'] ="Shift Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_karyawan' => $id_karyawan,
          'id_station' => $id_station,
          'id_shift' => $id_shift,
       ];
			$this->Job_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Job_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_job');
    $id_karyawan	 = $this->input->post('e_id_karyawan');
    $id_station	 = $this->input->post('e_id_station');
    $id_shift	 = $this->input->post('e_id_shift');
    if ($id_karyawan == '') {
      $result['pesan'] ="Karyawan Harus Diisi";
    }elseif ($id_station == '') {
      $result['pesan'] ="Station Harus Diisi";
    }elseif ($id_shift == '') {
      $result['pesan'] ="Shift Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_karyawan' => $id_karyawan,
          'id_station' => $id_station,
          'id_shift' => $id_shift,
       ];
			$this->Job_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Seksi_model->hapus($id);
	}
	public function test()
	{
		$data = $this->Job_model->view();
		var_dump($data);
	}
}
