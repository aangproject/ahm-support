<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jabatan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Jabatan_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Jabatan";
    $this->load->view('templates/header',$data);
    $this->load->view('jabatan/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Jabatan_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $nama_jabatan	 = $this->input->post('nama_jabatan');
    if ($nama_jabatan == '') {
      $result['pesan'] ="Jabatan Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_jabatan' => $nama_jabatan,
       ];
			$this->Jabatan_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Jabatan_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_jabatan');
    $nama_jabatan	 = $this->input->post('e_nama_jabatan');
    if ($nama_jabatan == '') {
      $result['pesan'] ="Jabatan Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_jabatan' => $nama_jabatan,
       ];
			$this->Jabatan_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Jabatan_model->hapus($id);
	}
}
