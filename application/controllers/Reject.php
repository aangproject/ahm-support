<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reject extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Reject_model');
		$this->load->model('Station_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Reject";
    $this->load->view('templates/header',$data);
    $this->load->view('reject/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Reject_model->view();
		echo json_encode($data);
	}

  public function getStation()
	{
		$data = $this->Station_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $id_station	 = $this->input->post('id_station');
    $reject	 = $this->input->post('reject');
    if ($id_station == '') {
      $result['pesan'] ="Nama Station Harus Diisi";
    }elseif ($reject == '') {
      $result['pesan'] ="Nama Reject Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_station' => $id_station,
          'reject' => $reject,
       ];
			$this->reject_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Reject_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_reject');
    $id_station	 = $this->input->post('e_id_station');
    $no_mesin	 = $this->input->post('e_reject');
    if ($id_station == '') {
      $result['pesan'] ="Nama Station Harus Diisi";
    }elseif ($reject == '') {
      $result['pesan'] ="Nama Reject Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
        'id_station' => $id_station,
        'reject' => $reject,
       ];
			$this->Reject_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Reject_model->hapus($id);
	}
}
