<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trouble extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Trouble_model');
		$this->load->model('Downtime_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Trouble";
    $this->load->view('templates/header',$data);
    $this->load->view('trouble/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Trouble_model->view();
		echo json_encode($data);
	}
	public function getDowntime()
	{
		$data = $this->Downtime_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $id_downtime	 = $this->input->post('id_downtime');
    $trouble	 = $this->input->post('trouble');
    if ($id_downtime == '') {
      $result['pesan'] ="Downtime Harus Diisi";
    }elseif ($trouble == '') {
      $result['pesan'] ="Trouble Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_downtime' => $id_downtime,
          'trouble' => $trouble,
       ];
			$this->Trouble_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Trouble_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_trouble');
    $id_downtime	 = $this->input->post('e_id_downtime');
    $trouble	 = $this->input->post('e_trouble');
    if ($id_downtime == '') {
      $result['pesan'] ="Downtime Harus Diisi";
    }elseif ($trouble == '') {
      $result['pesan'] ="Trouble Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_downtime' => $id_downtime,
          'trouble' => $trouble,
       ];
			$this->Trouble_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Trouble_model->hapus($id);
	}
}
