<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Downtime extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Downtime_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Downtime";
    $this->load->view('templates/header',$data);
    $this->load->view('downtime/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Downtime_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $downtime	 = $this->input->post('downtime');
    if ($downtime == '') {
      $result['pesan'] ="Downtime Type Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'downtime' => $downtime,
       ];
			$this->Downtime_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Downtime_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_downtime');
    $downtime	 = $this->input->post('e_downtime');
    if ($downtime == '') {
      $result['pesan'] ="Downtime Type Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'downtime' => $downtime,
       ];
			$this->Downtime_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Downtime_model->hapus($id);
	}
}
