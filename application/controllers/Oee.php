<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oee extends CI_Controller {
  function __construct()
  {
      parent::__construct();
      //load model
      $this->load->model('Auth_model');
      $this->load->model('Oee_model');

      // $this->load->model('Beranda_model');
      if (!$this->session->userdata('username')) {
              $this->session->set_flashdata('error', 'Anda belum melakukan login!');
              redirect('auth');
          }

  }
	public function index()
	{

    $data['title'] = 'OEE';
    $data['user'] = $this->Auth_model->success_login();
    $data['job'] = $this->getjob();
    $this->load->view('templates/header', $data);
    $this->load->view('oee/index', $data);
    $this->load->view('templates/footer');
	}

  public function tambah()
	{
    $nrp	 = $this->input->post('nrp');
    $nama	 = $this->input->post('nama');
    $shift	 = $this->input->post('shift');
    $station	 = $this->input->post('station');
    $no_mesin	 = $this->input->post('no_mesin');
    $type	 = $this->input->post('type');
    $dies	 = $this->input->post('dies');
    $actual	 = $this->input->post('actual');
    $sozai	 = $this->input->post('sozai');
    $ng	 = $this->input->post('ng');
    $downtime	 = $this->input->post('downtime');
    $tgl = date('Y-m-d');
    $ok = $actual-($sozai+$ng);

    if ($actual == '') {
      $result['pesan'] ="actual Harus Diisi";
    }elseif ($sozai == '') {
      $result['pesan'] ="sozai Harus Diisi";
    }elseif ($ng == '') {
      $result['pesan'] ="sozai Harus Diisi";
    }elseif ($ok < 0) {
      $result['pesan'] ="Error Input";
    }else {
      $result['pesan'] ="";
      $data = [
        
          'nama_station' => $station,
          'nama_shift' => $shift,
          'nama_type' => $type,
          'no_mesin' => $no_mesin,
          'dies' => $dies,
          'actual' => $actual,
          'sozai' => $sozai,
          'ng' => $ng,
          'ok' => $ok,
          'nama' => $nama,
          'nrp' => $nrp,
          'downtime' => $downtime,
          'tanggal' => $tgl,
       ];
			$this->Oee_model->tambah($data);

		}
		echo json_encode($result);
	}
  public function view()
  {
    $data['user'] = $this->Auth_model->success_login();
    $nrp = $data['user']['nrp']; 
    $data = $this->Oee_model->view($nrp);
    echo json_encode($data);
  }
  public function getidoee()
  {
    $data = $this->Oee_model->getLastId();
    echo json_encode($data);
  }
  public function getJob()
	{
    $user= $this->Auth_model->success_login();
    $id=$user['id']; 
    $jobId = $this->Oee_model->getIdJob($id);
    $getId = $jobId['id'];
		$data = $this->Oee_model->job($getId);
    return $data;
	}
  public function result()
  {
    $data = $this->getJob();
    echo json_encode($data);
  }

}
