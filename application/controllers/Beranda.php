<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
  function __construct()
  {
      parent::__construct();
      //load model
      $this->load->model('Auth_model');
      $this->load->model('Beranda_model');
      if (!$this->session->userdata('username')) {
              $this->session->set_flashdata('error', 'Anda belum melakukan login!');
              redirect('auth');
          }

  }
	public function index()
	{
    $data['title'] = "Beranda";
    $data['user'] = $this->Auth_model->success_login();
		$this->load->view('templates/header',$data);
    $this->load->view('beranda/index',$data);
    $this->load->view('templates/footer');
	}


}
