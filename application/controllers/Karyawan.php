<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jabatan_model');
		$this->load->model('Seksi_model');
		$this->load->model('Karyawan_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Karyawan";
    $this->load->view('templates/header',$data);
    $this->load->view('karyawan/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Karyawan_model->view();
		echo json_encode($data);
	}
	public function getJabatan()
	{
		$data = $this->Jabatan_model->view();
		echo json_encode($data);
	}
	public function getSeksi()
	{
		$data = $this->Seksi_model->view();
		echo json_encode($data);
	}
	public function tambah()
	{
    $nrp	 = $this->input->post('nrp');
    $nama	 = $this->input->post('nama');
    $id_seksi	 = $this->input->post('id_seksi');
    $password	 = $this->input->post('password');
    $id_jabatan	 = $this->input->post('id_jabatan');
    if ($nrp == '') {
      $result['pesan'] ="NRP Harus Diisi";
    }elseif ($nama == '') {
      $result['pesan'] ="Nama Harus Diisi";
    }elseif ($id_seksi == '') {
      $result['pesan'] ="Seksi Harus Diisi";
    }elseif ($password == '') {
      $result['pesan'] ="password Harus Diisi";
    }elseif ($id_jabatan == '') {
      $result['pesan'] ="Jabatan Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nrp' => $nrp,
          'nama' => $nama,
          'id_seksi' => $id_seksi,
          'password' => password_hash($password, PASSWORD_DEFAULT),
          'id_jabatan' => $id_jabatan,
       ];
			$this->Karyawan_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Karyawan_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_karyawan');
    $nrp	 = $this->input->post('e_nrp');
    $nama	 = $this->input->post('e_nama');
    $id_seksi	 = $this->input->post('e_id_seksi');
    $password	 = $this->input->post('e_password');
    $id_jabatan	 = $this->input->post('e_id_jabatan');
    if ($nrp == '') {
      $result['pesan'] ="NRP Harus Diisi";
    }elseif ($nama == '') {
      $result['pesan'] ="Nama Harus Diisi";
    }elseif ($id_seksi == '') {
      $result['pesan'] ="Seksi Harus Diisi";
    }elseif ($id_jabatan == '') {
      $result['pesan'] ="Jabatan Harus Diisi";
    }else {
      if ($password != '') {
        $data = [
            'nrp' => $nrp,
            'nama' => $nama,
            'id_seksi' => $id_seksi,
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'id_jabatan' => $id_jabatan,
         ];
      }else {
        $data = [
            'nrp' => $nrp,
            'nama' => $nama,
            'id_seksi' => $id_seksi,
            'id_jabatan' => $id_jabatan,
         ];
      }
      $result['pesan'] ="";
			$this->Karyawan_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Karyawan_model->hapus($id);
	}
	public function test()
	{
		$data = $this->Karyawan_model->view();
		var_dump($data);
	}
}
