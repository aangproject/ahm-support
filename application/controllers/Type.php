<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Type extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Type_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Type";
    $this->load->view('templates/header',$data);
    $this->load->view('type/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Type_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $nama_type	 = $this->input->post('nama_type');
    if ($nama_type == '') {
      $result['pesan'] ="Nama Type Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_type' => $nama_type,
       ];
			$this->Type_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Type_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_type');
    $nama_type	 = $this->input->post('e_nama_type');
    if ($nama_type == '') {
      $result['pesan'] ="Nama Type Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_type' => $nama_type,
       ];
			$this->Type_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Type_model->hapus($id);
	}
}
