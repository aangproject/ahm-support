<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seksi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Seksi_model');
		$this->load->model('Gedung_model');
		$this->load->model('Plant_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Seksi";
    $this->load->view('templates/header',$data);
    $this->load->view('seksi/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Seksi_model->view();
		echo json_encode($data);
	}
	public function getGedung()
	{
		$data = $this->Gedung_model->view();
		echo json_encode($data);
	}
	public function getPlant()
	{
		$data = $this->Plant_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $id_plant	 = $this->input->post('id_plant');
    $id_gedung	 = $this->input->post('id_gedung');
    $nama_seksi	 = $this->input->post('nama_seksi');
    if ($id_plant == '') {
      $result['pesan'] ="Plant Harus Diisi";
    }elseif ($id_gedung == '') {
      $result['pesan'] ="Gedung Harus Diisi";
    }elseif ($nama_seksi == '') {
      $result['pesan'] ="Seksi Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_plant' => $id_plant,
          'id_gedung' => $id_gedung,
          'nama_seksi' => $nama_seksi,
       ];
			$this->Seksi_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Seksi_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_seksi');
    $id_plant	 = $this->input->post('e_id_plant');
    $id_gedung	 = $this->input->post('e_id_gedung');
    $nama_seksi	 = $this->input->post('e_nama_seksi');
    if ($id_plant == '') {
      $result['pesan'] ="Plant Harus Diisi";
    }elseif ($id_gedung == '') {
      $result['pesan'] ="Gedung Harus Diisi";
    }elseif ($nama_seksi == '') {
      $result['pesan'] ="Seksi Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_plant' => $id_plant,
          'id_gedung' => $id_gedung,
          'nama_seksi' => $nama_seksi,
       ];
			$this->Seksi_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Seksi_model->hapus($id);
	}
	public function test()
	{
		$data = $this->Seksi_model->view();
		var_dump($data);
	}
}
