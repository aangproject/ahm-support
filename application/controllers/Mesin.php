<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mesin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Mesin_model');
		$this->load->model('Type_model');
		$this->load->model('Line_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }
	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Mesin";
    $this->load->view('templates/header',$data);
    $this->load->view('mesin/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Mesin_model->view();
		echo json_encode($data);
	}
	public function getType()
	{
		$data = $this->Type_model->view();
		echo json_encode($data);
	}
	public function getLine()
	{
		$data = $this->Line_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $id_line	 = $this->input->post('id_line');
    $id_type	 = $this->input->post('id_type');
    $dies	 = $this->input->post('dies');
    $cek = $this->Mesin_model->checkIfExist($id_line);
    if ($id_line == '') {
      $result['pesan'] ="Line Harus Diisi";
    }elseif ($id_type == '') {
      $result['pesan'] ="Type Harus Diisi";
    }elseif ($dies == '') {
      $result['pesan'] ="Dies Harus Diisi";
    }elseif ($cek['id_line'] == $id_line) {
      $result['pesan'] ="Line Sudah Ada";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_line' => $id_line,
          'id_type' => $id_type,
          'dies' => $dies,
       ];
			$this->Mesin_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Mesin_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_mesin');
    $id_line	 = $this->input->post('e_id_line');
    $id_type	 = $this->input->post('e_id_type');
    $dies	 = $this->input->post('e_dies');
    if ($id_line == '') {
      $result['pesan'] ="Line Harus Diisi";
    }elseif ($id_type == '') {
      $result['pesan'] ="Type Harus Diisi";
    }elseif ($dies == '') {
      $result['pesan'] ="Dies Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_line' => $id_line,
          'id_type' => $id_type,
          'dies' => $dies,
       ];
			$this->Mesin_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Mesin_model->hapus($id);
	}
	public function test()
	{
		$data = $this->Mesin_model->view();
		var_dump($data);
	}
}
