<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shift extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Shift_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Shift";
    $this->load->view('templates/header',$data);
    $this->load->view('shift/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Shift_model->view();
		echo json_encode($data);
	}
  
	public function tambah()
	{
    $nama_shift	 = $this->input->post('nama_shift');
    if ($nama_shift == '') {
      $result['pesan'] ="Nama Shift Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_shift' => $nama_shift,
       ];
			$this->Shift_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Shift_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_shift');
    $nama_shift	 = $this->input->post('e_nama_shift');
    if ($nama_shift == '') {
      $result['pesan'] ="Nama Shift Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_shift' => $nama_shift,
       ];
			$this->Shift_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Shift_model->hapus($id);
	}
}
