<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Station extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Station_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Station";
    $this->load->view('templates/header',$data);
    $this->load->view('station/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Station_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $nama_station	 = $this->input->post('nama_station');
    if ($nama_station == '') {
      $result['pesan'] ="Nama Station Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_station' => $nama_station,
       ];
			$this->Station_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Station_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_station');
    $nama_station	 = $this->input->post('e_nama_station');
    if ($nama_station == '') {
      $result['pesan'] ="Nama Station Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_station' => $nama_station,
       ];
			$this->Station_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Station_model->hapus($id);
	}
}
