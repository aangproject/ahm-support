<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gedung extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Gedung_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Gedung";
    $this->load->view('templates/header',$data);
    $this->load->view('gedung/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Gedung_model->view();
		echo json_encode($data);
	}
  
	public function tambah()
	{
    $nama_gedung	 = $this->input->post('nama_gedung');
    if ($nama_gedung == '') {
      $result['pesan'] ="Nama Gedung Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_gedung' => $nama_gedung,
       ];
			$this->Gedung_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Gedung_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_gedung');
    $nama_gedung	 = $this->input->post('e_nama_gedung');
    if ($nama_gedung == '') {
      $result['pesan'] ="Nama Gedung Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_gedung' => $nama_gedung,
       ];
			$this->Gedung_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Gedung_model->hapus($id);
	}
}
