<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Plant extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Plant_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Plant";
    $this->load->view('templates/header',$data);
    $this->load->view('plant/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Plant_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $nama_plant	 = $this->input->post('nama_plant');
    if ($nama_plant == '') {
      $result['pesan'] ="Nama Plant Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_plant' => $nama_plant,
       ];
			$this->Plant_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Plant_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_plant');
    $nama_plant	 = $this->input->post('e_nama_plant');
    if ($nama_plant == '') {
      $result['pesan'] ="Nama Plant Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_plant' => $nama_plant,
       ];
			$this->Plant_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Plant_model->hapus($id);
	}
}
