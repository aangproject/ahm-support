<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Line extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Line_model');
		$this->load->model('Station_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Line";
    $this->load->view('templates/header',$data);
    $this->load->view('line/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Line_model->view();
		echo json_encode($data);
	}
	public function getStation()
	{
		$data = $this->Station_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $id_station	 = $this->input->post('id_station');
    $no_mesin	 = $this->input->post('no_mesin');
    if ($id_station == '') {
      $result['pesan'] ="Station Harus Diisi";
    }elseif ($no_mesin == '') {
      $result['pesan'] ="No Mesin Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_station' => $id_station,
          'no_mesin' => $no_mesin,
       ];
			$this->Line_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Line_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_line');
    $id_station	 = $this->input->post('e_id_station');
    $no_mesin	 = $this->input->post('e_no_mesin');
    if ($id_station == '') {
      $result['pesan'] ="Station Harus Diisi";
    }elseif ($no_mesin == '') {
      $result['pesan'] ="No Mesin Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_station' => $id_station,
          'no_mesin' => $no_mesin,
       ];
			$this->Line_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Line_model->hapus($id);
	}
	public function test()
	{
		$data = $this->Line_model->view();
		var_dump($data);
	}
}
