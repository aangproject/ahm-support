<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
          $this->load->model('Auth_model');
        $this->load->helper('url');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }

        if ($this->session->userdata('level') == 2) {
            redirect('admin/blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['title'] = 'Admin | Manajemen user';
        $data['view'] = $this->User_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        $data['user'] = $this->Auth_model->success_login();
      // $data['user'] = $this->Auth_model->success_login();
      $data['title'] = 'Admin | Tambah User';

      $this->form_validation->set_rules('nama', 'Nama', 'required|trim', [
          'required' => 'nama tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[user.username]', [
          'required' => 'Username tidak boleh kosong!',
          'is_unique' => 'Username sudah digunakan!'
      ]);
      $this->form_validation->set_rules('email', 'Email', 'required|trim', [
          'required' => 'Email tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('level', 'Level', 'required|trim', [
          'required' => 'Level tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', [
          'matches' => 'Password tidak sesuai!',
          'min_length' => 'Password terlalu pendek!',
          'required' => 'Password tidak boleh kosong'
      ]);
      $this->form_validation->set_rules('password2', 'password', 'required|trim|matches[password1]');

      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('user/tambah', $data);
          $this->load->view('templates/footer');

      } else {

          $this->User_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('user');
      }
    }

    public function hapus($id)
    {
        $this->User_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('user');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['admin'] = $this->User_model->getById($id);
        $data['title'] = 'Admin | Ubah User';

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim', [
            'required' => 'nama tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'required|trim', [
            'required' => 'Email tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('level', 'Level', 'required|trim', [
            'required' => 'Level tidak boleh kosong!'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('user/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->User_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('user');
        }
    }
}
