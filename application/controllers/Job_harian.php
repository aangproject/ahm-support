<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Job_harian extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Job_harian_model');
		$this->load->model('Mesin_model');
		$this->load->model('Job_model');
    $this->load->model('Auth_model');
    if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('error', 'Anda belum melakukan login!');
            redirect('auth');
        }

	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Job Harian";
    $this->load->view('templates/header',$data);
    $this->load->view('job_harian/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Job_harian_model->view();
		echo json_encode($data);
	}
	public function getMesin()
	{
		$data = $this->Mesin_model->view();
		echo json_encode($data);
	}
	public function getJob()
	{
		$data = $this->Job_model->view();
		echo json_encode($data);
	}

	public function tambah()
	{
    $id_job	 = $this->input->post('id_job');
    $id_mesin	 = $this->input->post('id_mesin');
    if ($id_job == '') {
      $result['pesan'] ="Job Harus Diisi";
    }elseif ($id_mesin == '') {
      $result['pesan'] ="Mesin Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_job' => $id_job,
          'id_mesin' => $id_mesin,
       ];
			$this->Job_harian_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Job_harian_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_job_harian');
    $id_mesin = $this->input->post('e_id_mesin');
    $id_job	 = $this->input->post('e_id_job');
    if ($id_mesin == '') {
      $result['pesan'] ="Mesin Harus Diisi";
    }elseif ($id_job == '') {
      $result['pesan'] ="Job Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'id_mesin' => $id_mesin,
          'id_job' => $id_job,
       ];
			$this->Job_harian_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Job_harian_model->hapus($id);
	}
	public function test()
	{
		$data = $this->Job_harian_model->view();
		var_dump($data);
	}
}
