<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Auth_model');
    $this->load->helper('url');
	}

	public function index()
	{
		$this->form_validation->set_rules('nrp', 'NRP', 'required', [
			'required' => 'NRP tidak boleh kosong!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim', [
			'required' => 'Password tidak boleh kosong'
		]);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login Pengguna';

			$this->load->view('templates/auth_header', $data);
			$this->load->view('login/index');
			$this->load->view('templates/auth_footer');
		} else {
			$nrp = $this->input->post('nrp');
			$password = $this->input->post('password');

			$user = $this->Auth_model->login($nrp, $password);

			if ($user) {
					//cek password
					if (password_verify($password, $user['password'])) {
						$data = [
							'username' => $user['nrp'],
							'level' => $user['id_jabatan']
						];
						$this->session->set_userdata($data);
						redirect('beranda');
					} else {
						$this->session->set_flashdata('warning', 'Password salah, periksa kembali');
						redirect('auth');
					}
			} else {
				$this->session->set_flashdata('error', 'NRP belum terdaftar!');
				redirect('auth');
			}
		}
	}


	public function logout()
	{

		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		$this->session->set_flashdata('success', 'Anda telah logout');
		redirect('auth');
	}

}
