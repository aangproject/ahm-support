-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2021 at 07:35 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eform`
--

-- --------------------------------------------------------

--
-- Table structure for table `id_trouble _detail`
--

CREATE TABLE `id_trouble _detail` (
  `id` int(11) NOT NULL,
  `id_oee` int(11) NOT NULL,
  `id_trouble` int(11) NOT NULL,
  `waktu` varchar(225) NOT NULL,
  `menit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_downtime`
--

CREATE TABLE `tb_downtime` (
  `id` int(11) NOT NULL,
  `downtime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_downtime`
--

INSERT INTO `tb_downtime` (`id`, `downtime`) VALUES
(1, 'Start-Stop Lost Time'),
(2, 'MC Down Time'),
(3, 'Model Change'),
(4, 'Jig & Tool Change'),
(5, 'Part Delay');

-- --------------------------------------------------------

--
-- Table structure for table `tb_gedung`
--

CREATE TABLE `tb_gedung` (
  `id` int(11) NOT NULL,
  `nama_gedung` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_gedung`
--

INSERT INTO `tb_gedung` (`id`, `nama_gedung`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'E'),
(6, 'F'),
(7, 'G');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `id` int(11) NOT NULL,
  `nama_jabatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`id`, `nama_jabatan`) VALUES
(1, 'Section Head'),
(2, 'Foreman'),
(3, 'Quality Control'),
(4, 'Teknisi'),
(5, 'Operator');

-- --------------------------------------------------------

--
-- Table structure for table `tb_job`
--

CREATE TABLE `tb_job` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `id_station` int(11) NOT NULL,
  `id_shift` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_job`
--

INSERT INTO `tb_job` (`id`, `id_karyawan`, `id_station`, `id_shift`) VALUES
(1, 5, 1, 1),
(2, 6, 1, 2),
(3, 7, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tb_job_harian`
--

CREATE TABLE `tb_job_harian` (
  `id` int(11) NOT NULL,
  `id_job` int(11) NOT NULL,
  `id_mesin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_job_harian`
--

INSERT INTO `tb_job_harian` (`id`, `id_job`, `id_mesin`) VALUES
(1, 1, 2),
(2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `id` int(11) NOT NULL,
  `nrp` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `id_seksi` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_jabatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`id`, `nrp`, `nama`, `id_seksi`, `password`, `id_jabatan`) VALUES
(1, '32814', 'aang', 1, '$2y$10$w6dS0xDDsyjtQYi8O4nLNOoEeuKgqf6lX.AQveYLjWbthWoH4v2Um', 1),
(2, '17620', 'Purwanto', 1, '$2y$10$MvU3Z3Bh.UsPhFvZy98FwO1gjAp7bcBCmrohxk9mcE7bXrLYlLRfy', 2),
(3, '21001', 'Susilo', 1, '$2y$10$mOu/oTqUYVkWG.y9d.TDbO/NtD6ntpzxxRV1SF.ZoBNY/CuYTdIxK', 4),
(4, '17402', 'Cecep', 1, '$2y$10$UkgEHn7y4UKJNcMNzYIxEu7VXYiESRkkI0B/Mky9oYqxnDc/qw4by', 3),
(5, '32812', 'Arif', 1, '$2y$10$DJ0MVz18b/ZOBbCc5KqN6u7OnmMfcQQIMwobmHm1RdePOgmPo0eDy', 5),
(6, '31226', 'Yusli', 1, '$2y$10$Nrv6Jk7Yf4Z.DJm0MX91nuNGgHxa3Y6Y7fLwLujOzQz2XJQp4WjX.', 5),
(7, '35167', 'Ryana', 1, '$2y$10$PaAp1cPydtDYbqvRXcnmXeR9dWxsLL/8dip3DiaII4JhrED5Gph9y', 5),
(8, '44444', 'Anggi', 1, '$2y$10$w97e/TNeywtZONkHpG66Sep2qsggv3hI6WEPcILJMHwQZo2fHGOsu', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tb_line`
--

CREATE TABLE `tb_line` (
  `id` int(11) NOT NULL,
  `id_station` int(11) NOT NULL,
  `no_mesin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_line`
--

INSERT INTO `tb_line` (`id`, `id_station`, `no_mesin`) VALUES
(1, 1, '1'),
(2, 1, '2'),
(3, 1, '3'),
(4, 1, '4'),
(5, 1, '5'),
(6, 1, '6'),
(7, 1, '7'),
(8, 1, '8'),
(9, 1, '9'),
(10, 1, '10'),
(11, 1, '11'),
(12, 1, '12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mesin`
--

CREATE TABLE `tb_mesin` (
  `id` int(11) NOT NULL,
  `id_line` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `dies` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mesin`
--

INSERT INTO `tb_mesin` (`id`, `id_line`, `id_type`, `dies`) VALUES
(1, 1, 1, '1'),
(2, 2, 1, '2'),
(3, 3, 2, '1'),
(4, 4, 2, '2');

-- --------------------------------------------------------

--
-- Table structure for table `tb_oee_gdc`
--

CREATE TABLE `tb_oee_gdc` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nama_station` varchar(255) NOT NULL,
  `nama_shift` varchar(255) NOT NULL,
  `no_mesin` varchar(255) NOT NULL,
  `nrp` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nama_type` varchar(255) NOT NULL,
  `dies` varchar(255) NOT NULL,
  `actual` int(11) NOT NULL,
  `sozai` int(11) NOT NULL,
  `ng` int(11) NOT NULL,
  `downtime` int(11) NOT NULL,
  `ok` int(11) NOT NULL,
  `teknisi` varchar(255) NOT NULL,
  `foreman` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_oee_gdc`
--

INSERT INTO `tb_oee_gdc` (`id`, `tanggal`, `nama_station`, `nama_shift`, `no_mesin`, `nrp`, `nama`, `nama_type`, `dies`, `actual`, `sozai`, `ng`, `downtime`, `ok`, `teknisi`, `foreman`) VALUES
(1, '2021-05-02', 'GDC', 'Shift 1', '2', '32812', 'Arif', 'Front-K1AA', '2', 80, 2, 5, 20, 73, '', ''),
(2, '2021-05-02', 'GDC', 'Shift 1', '2', '32812', 'Arif', 'Front-K1AA', '2', 50, 1, 1, 40, 48, '', ''),
(3, '2021-05-02', 'GDC', 'Shift 1', '2', '32812', 'Arif', 'Front-K1AA', '2', 45, 1, 2, 120, 42, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_plant`
--

CREATE TABLE `tb_plant` (
  `id` int(11) NOT NULL,
  `nama_plant` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_plant`
--

INSERT INTO `tb_plant` (`id`, `nama_plant`) VALUES
(1, 'Plant-1'),
(2, 'Plant-2'),
(3, 'Plant-3'),
(4, 'Plant-3A');

-- --------------------------------------------------------

--
-- Table structure for table `tb_re`
--

CREATE TABLE `tb_re` (
  `id` int(11) NOT NULL,
  `id_oee` int(11) NOT NULL,
  `id_reject` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_reject`
--

CREATE TABLE `tb_reject` (
  `id` int(11) NOT NULL,
  `id_station` int(11) NOT NULL,
  `reject` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_reject`
--

INSERT INTO `tb_reject` (`id`, `id_station`, `reject`) VALUES
(1, 1, 'Misrun');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reject_detail`
--

CREATE TABLE `tb_reject_detail` (
  `id` int(11) NOT NULL,
  `id_oee` int(11) NOT NULL,
  `id_reject` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_seksi`
--

CREATE TABLE `tb_seksi` (
  `id` int(11) NOT NULL,
  `id_plant` int(11) NOT NULL,
  `id_gedung` int(11) NOT NULL,
  `nama_seksi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_seksi`
--

INSERT INTO `tb_seksi` (`id`, `id_plant`, `id_gedung`, `nama_seksi`) VALUES
(1, 4, 6, 'Die Casting CW'),
(2, 4, 6, 'Machining CW'),
(3, 4, 6, 'Painting CW');

-- --------------------------------------------------------

--
-- Table structure for table `tb_shift`
--

CREATE TABLE `tb_shift` (
  `id` int(11) NOT NULL,
  `nama_shift` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_shift`
--

INSERT INTO `tb_shift` (`id`, `nama_shift`) VALUES
(1, 'Shift 1'),
(2, 'Shift 2'),
(3, 'Shift 3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_station`
--

CREATE TABLE `tb_station` (
  `id` int(11) NOT NULL,
  `nama_station` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_station`
--

INSERT INTO `tb_station` (`id`, `nama_station`) VALUES
(1, 'GDC');

-- --------------------------------------------------------

--
-- Table structure for table `tb_trouble`
--

CREATE TABLE `tb_trouble` (
  `id` int(11) NOT NULL,
  `id_downtime` int(11) NOT NULL,
  `trouble` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trouble`
--

INSERT INTO `tb_trouble` (`id`, `id_downtime`, `trouble`) VALUES
(1, 1, 'Part Nempel'),
(2, 1, 'Repair Under Cut'),
(3, 1, 'Repair Dies'),
(4, 2, 'Ls Core Abnormal'),
(5, 2, 'Temperatur Molten Low'),
(6, 2, 'Thermocople Rusak'),
(7, 3, 'Loading'),
(8, 3, 'Unloading'),
(9, 3, 'Preheating'),
(10, 4, 'Ganti Lounder'),
(11, 4, 'Ganti Pouring Ladle');

-- --------------------------------------------------------

--
-- Table structure for table `tb_type`
--

CREATE TABLE `tb_type` (
  `id` int(11) NOT NULL,
  `nama_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_type`
--

INSERT INTO `tb_type` (`id`, `nama_type`) VALUES
(1, 'Front-K1AA'),
(2, 'Rear-K1AA'),
(3, 'Front-KWW'),
(4, 'Rear-KWW');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `email`, `status`, `password`, `level`) VALUES
(1, 'admin', 'admin', 'admin@mail.com', 1, '$2y$10$lfpOn2dMlq7NGCfIYzqwx.qiE6g70epeU1/ybDWVpfDF0B5zTVdVa', 1),
(2, 'Aang', 'aang', 'aang@mail.com', 1, '$2y$10$Zi5X.5QZVWu5QN8xbJr4p.fxW0Z8E54XLLl/vmW6S9jiCcI3HiSMO', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_downtime`
--
ALTER TABLE `tb_downtime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_gedung`
--
ALTER TABLE `tb_gedung`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_job`
--
ALTER TABLE `tb_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_job_harian`
--
ALTER TABLE `tb_job_harian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_line`
--
ALTER TABLE `tb_line`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_mesin`
--
ALTER TABLE `tb_mesin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_oee_gdc`
--
ALTER TABLE `tb_oee_gdc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_plant`
--
ALTER TABLE `tb_plant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_re`
--
ALTER TABLE `tb_re`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_reject`
--
ALTER TABLE `tb_reject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_reject_detail`
--
ALTER TABLE `tb_reject_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_seksi`
--
ALTER TABLE `tb_seksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_shift`
--
ALTER TABLE `tb_shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_station`
--
ALTER TABLE `tb_station`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_trouble`
--
ALTER TABLE `tb_trouble`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_type`
--
ALTER TABLE `tb_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_downtime`
--
ALTER TABLE `tb_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_gedung`
--
ALTER TABLE `tb_gedung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_job`
--
ALTER TABLE `tb_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_job_harian`
--
ALTER TABLE `tb_job_harian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_line`
--
ALTER TABLE `tb_line`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_mesin`
--
ALTER TABLE `tb_mesin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_oee_gdc`
--
ALTER TABLE `tb_oee_gdc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_plant`
--
ALTER TABLE `tb_plant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_re`
--
ALTER TABLE `tb_re`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_reject`
--
ALTER TABLE `tb_reject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_reject_detail`
--
ALTER TABLE `tb_reject_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_seksi`
--
ALTER TABLE `tb_seksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_shift`
--
ALTER TABLE `tb_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_station`
--
ALTER TABLE `tb_station`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_trouble`
--
ALTER TABLE `tb_trouble`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_type`
--
ALTER TABLE `tb_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
